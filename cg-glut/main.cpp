#define GL_SILENCE_DEPRECATION

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

GLuint texture,texture_,texture_1,texture_2,texture_3,texture_4,texture_5,texture_6,texture_7,texture_8,texture_9;

int view = 2;
int gif = 0;
float fl = 0.0f;
bool draw_test = false;

// angle of rotation for the camera direction
float angle = 0.0f;

// actual vector representing the camera's direction
float lx=0.0f,lz=-5.0f;

// XZ position of the camera
float x=0.0f, z=5.0f;

// movement indicator
float deltaAngle = 0.0f;
float deltaMove = 0;
float deltaSide = 0;
int xOrigin = -1;

//render bool
bool render_type = false;
//fps coords
bool fps_show = false;
// width and height of the window
int h,w;

// variables to compute frames per second
int frame;
long time, timebase;
char s[50];

// licht posi und andere arrays welche zur schattenbrechnung genutzt werden
GLfloat position[] = { 5.0, 5.0, 15.0, 0.0 };
static GLfloat floorPlane[4];
static GLfloat floorShadow[4][4];
static GLfloat floorVertices[4][3] = {
    { -20.0, 0.0, 20.0 },
    { 20.0, 0.0, 20.0 },
    { 20.0, 0.0, -20.0 },
    { -20.0, 0.0, -20.0 },
};

enum {
    X, Y, Z, W
};
enum {
    A, B, C, D
};

void shadowMatrix(GLfloat shadowMat[4][4],GLfloat groundplane[4],GLfloat lightpos[4])
{
    GLfloat dot;
    
    /* Find dot product between light position vector and ground plane normal. */
    dot = groundplane[X] * lightpos[X] +
    groundplane[Y] * lightpos[Y] +
    groundplane[Z] * lightpos[Z] +
    groundplane[W] * lightpos[W];
    
    shadowMat[0][0] = dot - lightpos[X] * groundplane[X];
    shadowMat[1][0] = 0.f - lightpos[X] * groundplane[Y];
    shadowMat[2][0] = 0.f - lightpos[X] * groundplane[Z];
    shadowMat[3][0] = 0.f - lightpos[X] * groundplane[W];
    
    shadowMat[X][1] = 0.f - lightpos[Y] * groundplane[X];
    shadowMat[1][1] = dot - lightpos[Y] * groundplane[Y];
    shadowMat[2][1] = 0.f - lightpos[Y] * groundplane[Z];
    shadowMat[3][1] = 0.f - lightpos[Y] * groundplane[W];
    
    shadowMat[X][2] = 0.f - lightpos[Z] * groundplane[X];
    shadowMat[1][2] = 0.f - lightpos[Z] * groundplane[Y];
    shadowMat[2][2] = dot - lightpos[Z] * groundplane[Z];
    shadowMat[3][2] = 0.f - lightpos[Z] * groundplane[W];
    
    shadowMat[X][3] = 0.f - lightpos[W] * groundplane[X];
    shadowMat[1][3] = 0.f - lightpos[W] * groundplane[Y];
    shadowMat[2][3] = 0.f - lightpos[W] * groundplane[Z];
    shadowMat[3][3] = dot - lightpos[W] * groundplane[W];
    
}

void findPlane(GLfloat plane[4],GLfloat v0[3], GLfloat v1[3], GLfloat v2[3])
{
    GLfloat vec0[3], vec1[3];
    
    /* Need 2 vectors to find cross product. */
    vec0[X] = v1[X] - v0[X];
    vec0[Y] = v1[Y] - v0[Y];
    vec0[Z] = v1[Z] - v0[Z];
    
    vec1[X] = v2[X] - v0[X];
    vec1[Y] = v2[Y] - v0[Y];
    vec1[Z] = v2[Z] - v0[Z];
    
    /* find cross product to get A, B, and C of plane equation */
    plane[A] = vec0[Y] * vec1[Z] - vec0[Z] * vec1[Y];
    plane[B] = -(vec0[X] * vec1[Z] - vec0[Z] * vec1[X]);
    plane[C] = vec0[X] * vec1[Y] - vec0[Y] * vec1[X];
    
    plane[D] = -(plane[A] * v0[X] + plane[B] * v0[Y] + plane[C] * v0[Z]);
}


// lightning
void init() {
    
    GLfloat ambient[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat lmodel_ambient[] = { 0.4, 0.4, 0.4, 1.0 };
    GLfloat local_view[] = { 5 };
    
    glClearColor(0.0, 0.1, 0.1, 0.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    // keep colors
    glEnable ( GL_COLOR_MATERIAL ) ;
    
}

// load textures
GLuint LoadTexture( const char * filename, int width, int height )
{
    GLuint texture;
    unsigned char * data;
    FILE * file;
    
    //load raw file
    file = fopen(filename, "rb");
    if (file == NULL) return 0;
    // malloc size * 3 ch
    data = (unsigned char *)malloc(width * height * 3);
    fread(data, width * height * 3, 1, file);
    fclose(file);
    
    // generate and bind texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE);
    
    // quality to best without mipmapping
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    
    // repeat texture
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    
    // generate the texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    free(data);
    return texture;
}

void FreeTexture(GLuint texture)
{
    glDeleteTextures(1, &texture);
}
// end load textures

// drawings
namespace sky {

void draw() {
    
    glBindTexture(GL_TEXTURE_2D, texture_1);
    
    glBegin(GL_QUADS);
    // mid
    glTexCoord2f(1, 0);
    glVertex3f(-20.0f, 20.0f, -20.0f);
    glTexCoord2f(1, 1);
    glVertex3f(-20.0f, 0.0f,  -20.0f);
    glTexCoord2f(0, 1);
    glVertex3f( 20.0f, 0.0f,  -20.0f);
    glTexCoord2f(0, 0);
    glVertex3f( 20.0f, 20.0f, -20.0f);
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, texture_2);
    glBegin(GL_QUADS);
    // left
    glTexCoord2f(1, 0);
    glVertex3f(-20.0f, 20.0f, 20.0f);
    glTexCoord2f(1, 1);
    glVertex3f(-20.0f, 0.0f,  20.0f);
    glTexCoord2f(0, 1);
    glVertex3f( -20.0f, 0.0f,  -20.0f);
    glTexCoord2f(0, 0);
    glVertex3f( -20.0f, 20.0f, -20.0f);
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, texture_3);
    
    glBegin(GL_QUADS);
    glTexCoord2f(1, 0);
    glVertex3f(20.0f, 20.0f, -20.0f);
    glTexCoord2f(1, 1);
    glVertex3f(20.0f, 0.0f,  -20.0f);
    glTexCoord2f(0, 1);
    glVertex3f( 20.0f, 0.0f,  20.0f);
    glTexCoord2f(0, 0);
    glVertex3f( 20.0f, 20.0f, 20.0f);
    glEnd();
    
}
}
namespace ground {
void draw() {
    glBindTexture(GL_TEXTURE_2D, texture_);
    glBegin(GL_QUADS);
    glTexCoord3f(0, 0, 0);
    glVertex3fv(floorVertices[0]);
    glTexCoord3f(2, 0, 0);
    glVertex3fv(floorVertices[1]);
    glTexCoord3f(2, 2, 0);
    glVertex3fv(floorVertices[2]);
    glTexCoord3f(0, 2,0);
    glVertex3fv(floorVertices[3]);
    glEnd();
}
}
namespace cup {
void draw() {
    if(gif < 100 ) {
        glBindTexture(GL_TEXTURE_2D, texture_5);
        gif+=4;
    } else {
        glBindTexture(GL_TEXTURE_2D, texture_6);
        gif+=4;
        if (gif >= 200) {
            gif = 0;
        }
    }
    glDisable(GL_TEXTURE_2D);
    glPushMatrix();
    glRotatef(-90, 1, 0, 0);
    GLUquadricObj *quadratic = gluNewQuadric();
    glColor3f(1, 1, 1);
    gluCylinder(quadratic,0.2f,0.2f,0.5f,8,1);
    glPopMatrix();
    glEnable(GL_TEXTURE_2D);
    
    glPushMatrix();
    glBegin(GL_POLYGON);
    glColor3f(1, 1, 1);
    glTexCoord2f(0.5, 0);
    glVertex3f(0, 0.45, -0.2); // nord
    
    glVertex3f(0.125, 0.45, -0.125); // nord ost
    glTexCoord2f(0, 0.5);
    glVertex3f(0.2,0.45,0); // ost
    glTexCoord2f(0.5, 0.5);
    
    glVertex3f(0.125, 0.45, 0.125); // süd ost
    
    glVertex3f(0, 0.45, 0.2); // süd
    glTexCoord2f(0, 0.5);
    glVertex3f(-0.125, 0.45, 0.125); // süd west
    
    glVertex3f(-0.2,0.45,0); // west
    glTexCoord2f(0, 0);
    glVertex3f(-0.125, 0.45, -0.125); // nord west
    
    
    glEnd();
    glPopMatrix();
}
void shadow_draw() {
    
    glPushMatrix();
    glRotatef(-90, 1, 0, 0);
    GLUquadricObj *quadratic = gluNewQuadric();
    gluCylinder(quadratic,0.2f,0.2f,0.5f,8,1);
    glPopMatrix();
}

}
namespace boxes {

void draw() {
    //    fl+=0.1f;
    glBindTexture(GL_TEXTURE_2D, texture_4);
    //    glRotatef(fl, 0, 1, 0);
    glBegin(GL_QUADS);
    //Quad 1
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 2
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    //Quad 3
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    //Quad 4
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    //Quad 5
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 6
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glEnd();
    
}
void draw_helper(){
    glTranslatef(19, 1, -19);
    boxes::draw();
    glTranslatef(-2, 0, 0);
    boxes::draw();
    glTranslatef(-2, 0, 0);
    boxes::draw();
    glTranslatef(+2, +2, 0);
    boxes::draw();
    glTranslatef(-15, -3, +20);
}

void draw_rot() {
    fl+=0.1f;
    glTranslatef(2, 1, 0 );
    glBindTexture(GL_TEXTURE_2D, texture_4);
    glRotatef(fl, 0, 1, 0);
    glBegin(GL_QUADS);
    //Quad 1
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 2
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    //Quad 3
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    //Quad 4
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    //Quad 5
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 6
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glEnd();
}
}
namespace tree {
void draw() {
    GLUquadricObj *quadratic = gluNewQuadric();
    glColor3f(0.5f, 0.35f, 0.05f);
    gluCylinder(quadratic,0.2f,0.2f,3.0f,8,1);
    glTranslatef(0, 0, 3);
    glColor3f(0.0, 0.5, 0.0 );
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
    glColor3f(1, 1, 1);
}
void draw_helper() {
    glPushMatrix();
    glTranslatef(-20.0, 0, -19.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    for (int i = 0; i < 10; i++) {
        tree::draw();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 10; i++) {
        tree::draw();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-20.0, 0, -19.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 15; i++) {
        tree::draw();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 15; i++) {
        tree::draw();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-17.0, 0, -15.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 7; i++) {
        tree::draw();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 7; i++) {
        tree::draw();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-15.0, 0, -15.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 4; i++) {
        tree::draw();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-15.0, -3, 0 );
    for (int i = 0; i < 4; i++) {
        tree::draw();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-11.0, 0, -11.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 4; i++) {
        tree::draw();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-11.0, -3, 0 );
    for (int i = 0; i < 4; i++) {
        tree::draw();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
}

void draw_shadow() {
    GLUquadricObj *quadratic = gluNewQuadric();
    gluCylinder(quadratic,0.2f,0.2f,3.0f,8,1);
    glTranslatef(0, 0, 3);
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
    glTranslatef(0, 0, 0.7);
    glutSolidCone(2, 3, 16, 1);
}
void draw_helper_shadow() {
    glPushMatrix();
    glTranslatef(-20.0, 0, -19.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    for (int i = 0; i < 10; i++) {
        tree::draw_shadow();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 10; i++) {
        tree::draw_shadow();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-20.0, 0, -19.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 15; i++) {
        tree::draw_shadow();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 15; i++) {
        tree::draw_shadow();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-17.0, 0, -15.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 7; i++) {
        tree::draw_shadow();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-19.0, -3, 0 );
    for (int i = 0; i < 7; i++) {
        tree::draw_shadow();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-15.0, 0, -15.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 4; i++) {
        tree::draw_shadow();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-15.0, -3, 0 );
    for (int i = 0; i < 4; i++) {
        tree::draw_shadow();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-11.0, 0, -11.0 );
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-90, 0, 0, 1);
    
    for (int i = 0; i < 4; i++) {
        tree::draw_shadow();
        glTranslatef(2, sin(i), -5.1 );
    }
    glTranslatef(-11.0, -3, 0 );
    for (int i = 0; i < 4; i++) {
        tree::draw_shadow();
        glTranslatef(2, cos(i), -5.1 );
    }
    glPopMatrix();
}

}
namespace house {
void draw() {
    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, texture_7);
    glTranslatef(6, 1, -20);
    glScalef(4, 5, 1);
    glBegin(GL_QUADS);
    //Quad 1
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 2
    glTexCoord2f(1, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    glTexCoord2f(1, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    //Quad 3
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    //Quad 4
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    //Quad 5
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f, 1.0f,-1.0f);   //V6
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f, 1.0f, 1.0f);   //V8
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f, 1.0f, 1.0f);   //V2
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f, 1.0f,-1.0f);   //V4
    //Quad 6
    glTexCoord2f(1, 1);
    glVertex3f(-1.0f,-1.0f, 1.0f);   //V7
    glTexCoord2f(1, 0);
    glVertex3f(-1.0f,-1.0f,-1.0f);   //V5
    glTexCoord2f(0, 0);
    glVertex3f( 1.0f,-1.0f,-1.0f);   //V3
    glTexCoord2f(0, 1);
    glVertex3f( 1.0f,-1.0f, 1.0f);   //V1
    glEnd();
    glPopMatrix();
    
    
    glPushMatrix();
           glBindTexture(GL_TEXTURE_2D, texture_8);
    glTranslatef(6, 5.8, -19.5);
    glScalef(5, 1, 1.75);
    glBegin(GL_TRIANGLE_FAN);
    // the commond point of the four triangles
        glTexCoord2f(1, 1);
    glVertex3f(0, 1.4, 0);

    // Base points of each triangle
        glTexCoord2f(1, 0);
    glVertex3f(-1, 0, -1);
        glTexCoord2f(0, 0);
    glVertex3f(-1, 0,  1);
     glTexCoord2f(1, 0);
    glVertex3f(-1, 0,  1);
            glTexCoord2f(0, 0);
    glVertex3f( 1, 0,  1);
     glTexCoord2f(1, 0);
    glVertex3f( 1, 0,  1);
            glTexCoord2f(0, 0);
    glVertex3f( 1, 0, -1);
     glTexCoord2f(1, 0);
    glVertex3f( 1, 0, -1);
            glTexCoord2f(0, 0);
    glVertex3f(-1, 0, -1);

    glEnd();

    glPopMatrix();
}
}
namespace fenster {
void draw() {
    glPushMatrix();
    glTranslatef(7, 4.5, -18.4);
            glBindTexture(GL_TEXTURE_2D, texture_9);
    glBegin(GL_QUADS);
    //Quad 1
    glTexCoord2f(1, 1);
    glVertex3f( 0.5f, 0.5f,-0.5f);   //V4
    glTexCoord2f(1, 0);
    glVertex3f( 0.5f,-0.5f,-0.5f);   //V3
    glTexCoord2f(0, 0);
    glVertex3f(-0.5f,-0.5f,-0.5f);   //V5
    glTexCoord2f(0, 1);
    glVertex3f(-0.5f, 0.5f,-0.5f);
    glEnd();
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(4.5, 2.5, -18.4);
            glBindTexture(GL_TEXTURE_2D, texture_9);
    glBegin(GL_QUADS);
    //Quad 1
    glTexCoord2f(1, 1);
    glVertex3f( 0.5f, 0.5f,-0.5f);   //V4
    glTexCoord2f(1, 0);
    glVertex3f( 0.5f,-0.5f,-0.5f);   //V3
    glTexCoord2f(0, 0);
    glVertex3f(-0.5f,-0.5f,-0.5f);   //V5
    glTexCoord2f(0, 1);
    glVertex3f(-0.5f, 0.5f,-0.5f);
    glEnd();
    glPopMatrix();
}
}
// end drawings

// dynamic windowsize
void changeSize(int ww, int hh) {
    
    h = hh;
    w = ww;
    
    float ratio =  w * 1.0 / h;
    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);
    // Reset Matrix
    glLoadIdentity();
    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);
    // Set the correct perspective.
    gluPerspective(45.0f, ratio, 0.1f, 100.0f);
    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}
// fps
void fps_bitmap(float x,float y,float z,void *font,char *string) {
    
    char *c;
    glRasterPos3f(x,y,z);
    for (c=string; *c != '\0'; c++) {
        glutBitmapCharacter(font, *c);
    }
}

void restorePerspectiveProjection() {
    glMatrixMode(GL_PROJECTION);
    // restore previous projection matrix
    glPopMatrix();
    // get back to modelview mode
    glMatrixMode(GL_MODELVIEW);
}

void setOrthographicProjection() {
    // switch to projection mode
    glMatrixMode(GL_PROJECTION);
    // save previous matrix which contains the
    //settings for the perspective projection
    glPushMatrix();
    // reset matrix
    glLoadIdentity();
    // set a 2D orthographic projection
    gluOrtho2D(0, w, h, 0);
    // switch back to modelview mode
    glMatrixMode(GL_MODELVIEW);
}
// fps end
// movement
void computePos(float deltaMove) {
    if ((z + 0.5 == 20 && deltaMove < 0) || (z - 0.5 == -20 && deltaMove > 0)) {
        //nothing
    } else {
        z += deltaMove * lz * 0.1f;
        x += deltaMove * lx * 0.1f;
    }
    //    printf("x:%f\tz:%f\tdelta:%f\n",x,z,deltaMove);
}

void computeSide(float deltaSide) {
    
    //    printf("x:%f\tz:%f\tdelta:%f\n",x,z,deltaSide);
    if((x + 0.5 == 20 && deltaSide < 0) || (x- 0.5 == -20 && deltaSide > 0)) {
        //nothing
    } else {
        if (deltaSide == 1) {
            x-= 0.25f;
        }
        if (deltaSide == -1) {
            x+= 0.25f;
        }
    }
}
//movement end
void renderScene(void) {
    
    // forward and backward
    if (deltaMove)
        computePos(deltaMove);
    // left and right
    if (deltaSide) {
        computeSide(deltaSide);
    }
    // sachen füs licht
    GLfloat no_mat[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat mat_diffuse[] = { 1, 1, 1, 1.0 };
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat shininess[] = { 50.0 };
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Reset transformations
    glLoadIdentity();
    // Set the camera
    gluLookAt(  x,      view,   z,
              x+lx,   1.0f,   z+lz,
              0.0f,  1.0f,   0.0f);
    // sachen fürs licht
    glMaterialfv(GL_FRONT, GL_AMBIENT, no_mat);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
    
    glEnable(GL_TEXTURE_2D);
    // alles mit texturen laden
    ground::draw();
    sky::draw();
    boxes::draw_helper();
    cup::draw();
    if (draw_test) {
        glPushMatrix();
        boxes::draw_rot();
        glPopMatrix();
    }
    house::draw();
    fenster::draw();
    glDisable(GL_TEXTURE_2D);
    // alles ohne textruen laden
    tree::draw_helper();
    
    
    //schatten berechnen
    shadowMatrix(floorShadow, floorPlane, position);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_LIGHTING);
    glColor4f(0.0, 0.0, 0.0, 0.5); // 50% transparenz
    
    glPushMatrix();
    glTranslatef(0, 0.01, 0);
    glMultMatrixf((GLfloat *) floorShadow);
    //schatten zeichen
    cup::shadow_draw();
    tree::draw_helper_shadow();
    if (draw_test) {
        boxes::draw_rot();
    }
    glPopMatrix();
    glColor4f(1, 1, 1, 1);
    glDisable(GL_BLEND);
    glEnable(GL_LIGHTING);
    
    
    // compute and show fps
    if (fps_show) {
        frame++;
        time=glutGet(GLUT_ELAPSED_TIME);
        if (time - timebase > 1000) {
            sprintf(s,"FPS:%4.2f",
                    frame*1000.0/(time-timebase));
            timebase = time;
            frame = 0;
        }
        
        // show fps with bitmap fonts
        setOrthographicProjection();
        glPushMatrix();
        glLoadIdentity();
        fps_bitmap(0,10,0,GLUT_BITMAP_HELVETICA_18,s);
        glPopMatrix();
        restorePerspectiveProjection();
    }
    
    glFlush();
    glutSwapBuffers();
}
// keys
void processNormalKeys(unsigned char key, int xx, int yy) {
    
    switch (key) {
            // quit on ecs
        case 27:
            exit(0);
            break;
            // change render type on r
        case 82:
        case 114:
            if (!render_type) {
                glPolygonMode(GL_FRONT, GL_LINE); // wires
                render_type = true;
            } else {
                glPolygonMode(GL_FRONT, GL_FILL); // solid
                render_type = false;
            }
            break;
            // show / hide fps on f
        case 70:
        case 102:
            fps_show = !fps_show;
            break;
            
        case 49:
            view = 1;
            break;
        case 50:
            view = 2;
            break;
        case 51:
            view = 20;
            break;
        case 77:
        case 109:
            draw_test = !draw_test;
            break;
    }
    
}

void pressKey(int key, int xx, int yy) {
    
    switch (key) {
        case GLUT_KEY_UP : deltaMove = 0.5f; break;
        case GLUT_KEY_DOWN : deltaMove = -0.5f; break;
        case GLUT_KEY_RIGHT: deltaSide = -1; break;
        case GLUT_KEY_LEFT: deltaSide = 1; break;
    }
}

void releaseKey(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_UP :
        case GLUT_KEY_DOWN : deltaMove = 0;break;
        case GLUT_KEY_RIGHT:
        case GLUT_KEY_LEFT: deltaSide = 0; break;
    }
}
// mouse disabled
void mouseMove(int x, int y) {
    
    // this will only be true when the left button is down
    if (xOrigin >= 0) {
        
        // update deltaAngle
        deltaAngle = (x - xOrigin) * 0.001f;
        
        // update camera's direction
        lx = sin(angle + deltaAngle);
        lz = -cos(angle + deltaAngle);
    }
}

void mouseButton(int button, int state, int x, int y) {
    
    // only start motion if the left button is pressed
    if (button == GLUT_LEFT_BUTTON) {
        
        // when the button is released
        if (state == GLUT_UP) {
            angle += deltaAngle;
            xOrigin = -1;
        }
        else  {// state = GLUT_DOWN
            xOrigin = x;
        }
    }
}
// programm loop
int main(int argc, char **argv) {
    
    // init GLUT and create window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(400,00);
    glutInitWindowSize(1024,1024);
    glutCreateWindow("simple proj");
    
    // register callbacks
    glutDisplayFunc(renderScene);
    glutReshapeFunc(changeSize);
    glutIdleFunc(renderScene);
    
    glutIgnoreKeyRepeat(1);
    glutKeyboardFunc(processNormalKeys);
    glutSpecialFunc(pressKey);
    glutSpecialUpFunc(releaseKey);
    
    //                glutMouseFunc(mouseButton);
    //                glutMotionFunc(mouseMove);
    
    // init
    init();
    findPlane(floorPlane, floorVertices[1], floorVertices[2], floorVertices[3]);
    
    // loading textures
    texture = LoadTexture("test.raw", 256, 256);
    texture_ = LoadTexture( "grass.raw", 512, 512 );
    texture_1 = LoadTexture( "sky_f.raw", 512, 512 );
    texture_2 = LoadTexture( "sky_r.raw", 512, 512 );
    texture_3 = LoadTexture( "sky_l.raw", 512, 512 );
    texture_4 = LoadTexture( "box.raw", 512, 512 );
    texture_5 = LoadTexture("water_1.raw", 512, 512);
    texture_6 = LoadTexture("water_2.raw", 512, 512);
    texture_7 = LoadTexture("bricks.raw", 512, 512);
    texture_8 = LoadTexture("dach.raw", 512, 512);
        texture_9 = LoadTexture("fenster.raw", 512, 512);
    glutMainLoop();
    FreeTexture(texture);
    FreeTexture(texture_);
    FreeTexture(texture_1);
    FreeTexture(texture_2);
    FreeTexture(texture_3);
    FreeTexture(texture_4);
    FreeTexture(texture_5);
    FreeTexture(texture_6);
    FreeTexture(texture_7);
    FreeTexture(texture_8);
    FreeTexture(texture_9);
    
    return 1;
}
